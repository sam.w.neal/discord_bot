import bot
import discord
from discord.ext import commands


class ChannelBan(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(
        pass_context=True, aliases=["vbanhammer", "vban", "vb", "voiceban"]
    )
    @commands.guild_only()
    @commands.has_role(299758677890695169)
    async def voice_ban(self, ctx, user_mentioned: discord.User = None):
        banned_users = bot.db["banned_users"]
        banned_users.upsert(
            dict(user_id=user_mentioned.id, banned_by=ctx.author.id), ["user_id"]
        )
        server = self.bot.get_guild(299756881004462081)
        member = server.get_member(user_mentioned.id)
        try:
            await member.move_to(None)
        except:
            # Member probably isn't in a voice channel
            pass

        await ctx.send(
            f"{user_mentioned.mention} has been voice banned by {ctx.author.mention}."
        )

    @commands.command(
        pass_context=True, aliases=["unvbanhammer", "unvban", "uvb", "unvoiceban"]
    )
    @commands.guild_only()
    @commands.has_role(299758677890695169)
    async def un_voice_ban(self, ctx, user_mentioned: discord.User = None):
        banned_users = bot.db["banned_users"]

        if not banned_users.find_one(user_id=user_mentioned.id):
            await ctx.send(f"{user_mentioned.mention} is not voice banned")
        else:
            banned_users.delete(user_id=user_mentioned.id)
            await ctx.send(
                f"{user_mentioned.mention} has been un-voice banned by {ctx.author.mention}"
            )

    @commands.Cog.listener()
    async def on_voice_state_update(self, member, before, after):
        # Detect voice channel state change
        if (
            after.channel
            and after.channel is not after.channel.guild.afk_channel
            and member.id != self.bot.user.id
        ):
            banned_users = [user["user_id"] for user in bot.db["banned_users"]]
            server = self.bot.get_guild(299756881004462081)

            to_ban = [
                user.id for user in after.channel.members if user.id in banned_users
            ]
            # VOICE BAN
            for user in to_ban:
                member = server.get_member(user)
                await member.move_to(None)


def setup(bot):
    bot.add_cog(ChannelBan(bot))

from discord import VoiceChannel


def pprint_pendulum_duration(duration):
    if duration.days > 0:
        pretty = (
            f"{duration.days} days, "
            f"{duration.hours}:"
            f"{duration.minutes}:"
            f"{duration.remaining_seconds}"
        )
    else:
        pretty = f"{duration.hours}:{duration.minutes}:{duration.remaining_seconds}"
    return pretty


def build_s3_url(name):
    return f"https://scamdiscordbot2021.s3-ap-southeast-2.amazonaws.com/{name}.mp3"


def as_code(content):
    return f"```{content}```"


def get_all_voice_members(server):
    return [
        member
        for member in server.members
        if member.voice
        and isinstance(member.voice.channel, VoiceChannel)
        and member.voice.channel != server.afk_channel
    ]

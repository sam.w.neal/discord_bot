from discord.ext import commands
import bot


class Checks(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    def is_allowed_channel_check(ctx):
        channel = ctx.channel.id
        return channel in bot.allowed_channels


def setup(bot):
    bot.add_cog(Checks(bot))

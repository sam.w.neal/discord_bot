import discord
import asyncio
import json
import dataset
import requests
import logging
import aiologger
import traceback
import functools
from aiologger.loggers.json import JsonLogger
from collections import namedtuple
from cogs import checks, voice
from cogs.utils.utils import get_all_voice_members, as_code
from discord.ext import commands

# Get Config
with open("/discord_bot/config.json") as json_config:
    config = json.load(json_config)

client = discord.Client()


class DiscordHandler(aiologger.handlers.base.Handler):
    def __init__(self):
        aiologger.handlers.base.Handler.__init__(self)

    async def emit(self, record):
        await bot.wait_until_ready()
        try:
            message = f"{traceback.format_tb(record.exc_info[2])} \r {record.exc_info[0].__name__}: {record.exc_info[1]}".replace(
                r"\n", "\r"
            )
            await send_to_discord(as_code(message))
        except Exception as e:
            await self.handle_error(record, e)

    async def close(self) -> None:
        raise NotImplementedError("close must be implemented by Handler subclasses")


def log(function):
    @functools.wraps(function)
    def wrapper(*args, **kwargs):
        try:
            return function(*args, **kwargs)
        except Exception:
            error = "There was an exception in  "
            error += f"{function.__name__}()"
            logger.exception(error)

    return wrapper


async def send_to_discord(message, channel_id=int(config["LOG_CHANNEL_ID"])):
    channel = bot.get_channel(channel_id)
    await channel.send(message)


logger = JsonLogger.with_default_handlers(name="discord", loop=client.loop)
logger.setLevel = logging.ERROR
logger.add_handler(DiscordHandler())

bot = commands.Bot(command_prefix=["!", "."])

# Initialise database connection
db = dataset.connect("sqlite:////discord_bot/sqlite3/discord.db")

# this specifies what extensions to load when the bot starts up
startup_extensions = [
    "cogs.checks",
    "cogs.voice",
    "cogs.uploads",
    "cogs.bans",
]
allowed_channels = [414701519733260288, 471262806533079041]  # botspam  # bot


@log
def get_user_roles():
    users = bot.get_guild(299756881004462081).members
    user_roles_list = {}
    for user in users:
        user_roles_list[user.name] = [
            role.name for role in user.roles if role.name != ""
        ]

    return user_roles_list


@log
def update_user_roles(user_roles):
    db["user_roles"].drop()
    for user, roles in user_roles.items():
        db["user_roles"].upsert(dict(user=user, roles=",".join(roles)), ["user"])


@log
@bot.event
async def on_ready():
    print(f"We have logged in as {bot.user}")
    update_user_roles(get_user_roles())
    db["sound_queue"].drop()


@log
@bot.check
def is_allowed_channel(ctx):
    return checks.Checks.is_allowed_channel_check(ctx)


@log
@bot.command()
async def load(ctx, extension_name):
    """Loads an extension."""
    try:
        bot.load_extension(f"cogs.{extension_name}")
    except (AttributeError, ImportError) as e:
        await bot.say(as_code(f"py\n{type(e).__name__}: {str(e)}\n"))
        return
    await bot.say(f"{extension_name} loaded.")


@log
@bot.command()
async def unload(ctx, extension_name):
    """Unloads an extension."""
    bot.unload_extension(f"cogs.{extension_name}")
    await ctx.send(f"{extension_name} unloaded.")


@log
@bot.command()
async def r(ctx):
    """ Reloads all extensions."""
    for extension in startup_extensions:
        try:
            bot.unload_extension(extension)
            print(f"Unloaded {extension}")
            bot.load_extension(extension)
            print(f"Loaded {extension}")
        except Exception as e:
            exc = f"{type(e).__name__}: {e}"
            print(f"Failed to load/unload extension {extension}\n{exc}")


@log
async def server_roles_loop():
    await bot.wait_until_ready()
    loop_iter = 300  # (seconds) Loop will run and increment stats on this value

    while not bot.is_closed:
        await asyncio.sleep(loop_iter)
        update_user_roles(get_user_roles())


# Load Cogs
if __name__ == "__main__":
    for extension in startup_extensions:
        try:
            bot.load_extension(extension)
            print(f"Loaded {extension}")
        except Exception as e:
            exc = f"{type(e).__name__}: {e}"
            print(f"Failed to load extension {extension}\n{exc}")

    bot.loop.create_task(server_roles_loop())
    bot.run(config["token"])

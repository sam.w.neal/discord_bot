import discord
import bot
import requests
import boto3
from discord.ext import commands


class Uploads(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_message(self, message):
        if message.channel.id != 574866821061017600:
            return

        try:
            file_name = message.attachments[0].filename
        except IndexError:
            if message.author.id != self.bot.user.id:
                await message.delete()
            return

        if file_name.endswith(".mp3"):
            if message.author.id != self.bot.user.id:
                mp3_attachment = message.attachments[0]
                embed = UploadEmbed(member=message.author, file=mp3_attachment)
                msg = await message.channel.send(embed=embed)

                try:
                    self.upload_attachment(mp3_attachment)
                    await msg.edit(embed=embed.success())
                except Exception:
                    await msg.edit(embed=embed.fail())
                finally:
                    await message.delete()

    def upload_attachment(self, attachment):
        request = requests.get(attachment.url, stream=True)
        session = boto3.Session(
            aws_access_key_id=bot.config["AWS_ACCESS_KEY_ID"],
            aws_secret_access_key=bot.config["AWS_SECRET_ACCESS_KEY"],
        )
        s3 = session.resource("s3")
        bucket = s3.Bucket(bot.config["AWS_STORAGE_BUCKET_NAME"])
        bucket.upload_fileobj(request.raw, attachment.filename)
        bucket.Object(attachment.filename).wait_until_exists()


class UploadEmbed(discord.Embed):
    def __init__(
        self, member, file, colour=discord.Colour.gold(), footer="Attempting upload...",
    ):
        super().__init__()
        self.title = f"{file.filename}"
        self.colour = colour
        self.avatar = member.avatar_url
        self.set_author(name=f"Uploaded by {member.name}", icon_url=member.avatar_url)
        self.set_footer(text=footer)

    def success(self):
        self.colour = discord.Colour.green()
        self.set_footer(text="Upload successful.")
        return self

    def fail(self):
        self.colour = discord.Colour.red()
        self.set_footer(text="Upload failed.")
        return self


def setup(bot):
    bot.add_cog(Uploads(bot))

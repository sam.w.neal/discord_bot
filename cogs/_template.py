import discord
import asyncio
from discord.ext import commands

class TemplateCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def hello(self, ctx):
        await ctx.send('Hello {}'.format(ctx.message.author.mention))

def setup(bot):
    bot.add_cog(TemplateCog(bot))
FROM python:3.7.3-slim

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY . /discord_bot

WORKDIR /discord_bot

RUN apt-get update && apt-get install -y ffmpeg

CMD ["python", "/discord_bot/bot.py"]
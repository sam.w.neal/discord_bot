import asyncio
import discord
import youtube_dl
import requests
import bot
import dataset
import random
from tabulate import tabulate
from boto.s3.connection import S3Connection
from discord.ext import commands
from pathlib import Path
from pprint import pprint
from cogs.utils import utils
from cogs.utils.utils import as_code

# Suppress noise about console usage from errors
youtube_dl.utils.bug_reports_message = lambda: ""

ytdl_format_options = {
    "format": "bestaudio/best",
    "restrictfilenames": True,
    "noplaylist": True,
    "nocheckcertificate": True,
    "ignoreerrors": False,
    "logtostderr": False,
    "quiet": True,
    "no_warnings": True,
    "default_search": "auto",
    "source_address": "0.0.0.0",
}

ffmpeg_options = {"options": "-vn"}
ytdl = youtube_dl.YoutubeDL(ytdl_format_options)


class YTDLSource(discord.PCMVolumeTransformer):
    def __init__(self, source, *, data, volume=0.5):
        super().__init__(source, volume)

        self.data = data

        self.title = data.get("title")
        self.url = data.get("url")

    @classmethod
    async def from_url(cls, url, *, loop=None, stream=True):
        loop = loop or asyncio.get_event_loop()
        data = await loop.run_in_executor(
            None, lambda: ytdl.extract_info(url, download=not stream)
        )

        if "entries" in data:
            # take first item from a playlist
            data = data["entries"][0]

        filename = data["url"] if stream else ytdl.prepare_filename(data)
        return cls(discord.FFmpegPCMAudio(filename, **ffmpeg_options), data=data)


class Voice(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    async def summon(self, ctx, channel: discord.VoiceChannel = None):
        if not channel:
            channel = ctx.author.voice.channel

        if ctx.voice_client:
            return await ctx.voice_client.move_to(channel)

        await channel.connect()

    async def join_sound_summon(self, channel: discord.VoiceChannel):
        return await channel.connect()

    async def play_sound(self, ctx, sound):
        # if not requests.head(utils.build_s3_url(sound)).ok:
        #    return await ctx.send(as_code(f"Cannot find sound: {sound}"))

        player = await YTDLSource.from_url(
            utils.build_s3_url(sound), loop=self.bot.loop
        )

        await self.summon(ctx)
        if ctx.voice_client.is_playing():
            ctx.voice_client.pause()

        ctx.voice_client.play(
            player, after=lambda e: self.disconnector(ctx.voice_client, self.bot)
        )

    async def play_join_sound(self, member, sound):
        if not self.bot.voice_clients:
            if not requests.head(utils.build_s3_url(sound)).ok:
                return

            player = await YTDLSource.from_url(
                utils.build_s3_url(sound), loop=self.bot.loop
            )

            voice_client = await self.join_sound_summon(member.voice.channel)
            if voice_client.is_playing():
                voice_client.pause()

            voice_client.play(
                player, after=lambda e: self.disconnector(voice_client, self.bot)
            )

    @commands.command(aliases=["s"])
    @commands.guild_only()
    async def sound(self, ctx, sound):
        await self.play_sound(ctx, sound)

    async def summon(self, ctx, channel: discord.VoiceChannel = None):
        if not channel:
            channel = ctx.author.voice.channel

        if ctx.voice_client:
            return await ctx.voice_client.move_to(channel)

        await channel.connect()

    @commands.command()
    @commands.guild_only()
    async def join(self, ctx, channel: discord.VoiceChannel = None):
        if not channel:
            channel = ctx.author.voice.channel

        if ctx.voice_client:
            return await ctx.voice_client.move_to(channel)

        await channel.connect()

    @commands.command()
    async def stop(self, ctx):
        await ctx.voice_client.disconnect()

    @staticmethod
    def disconnector(voice_client: discord.VoiceClient, bot: commands.Bot):
        coro = voice_client.disconnect()
        fut = asyncio.run_coroutine_threadsafe(coro, bot.loop)
        try:
            fut.result()
        except asyncio.CancelledError:
            pass

    @commands.command()
    @commands.guild_only()
    async def list(self, ctx):
        async with ctx.typing():
            conn = S3Connection(
                bot.config["AWS_ACCESS_KEY_ID"], bot.config["AWS_SECRET_ACCESS_KEY"]
            )
            bucket = conn.get_bucket(bot.config["AWS_STORAGE_BUCKET_NAME"])

            sounds_list = []
            sounds_list_string = []

            for key in bucket.list():
                sounds_list.append(str(key.name.split(".mp3")[0]).lower())

            sounds_list.sort()

            msg_chunks = []
            chunk = []
            for index, sound in enumerate(sounds_list, start=1):
                if len(str(chunk)) <= 1000 and index != len(sounds_list):
                    chunk.append(sound)
                else:
                    if index == len(sounds_list):
                        chunk.append(sound)
                    tabbed_chunk = tabulate(
                        [chunk[i : i + 4] for i in range(0, len(chunk), 4)],
                        tablefmt="plain",
                    )
                    msg_chunks.append(tabbed_chunk)
                    chunk = []
                    chunk.append(sound)

        for chunk in msg_chunks:
            await ctx.send(as_code(chunk))

    @commands.Cog.listener()
    async def on_voice_state_update(self, member, before, after):
        # Detect voice channel state change
        if (
            after.channel
            and after.channel != before.channel
            and after.channel is not after.channel.guild.afk_channel
            and member.id != self.bot.user.id
            and len(after.channel.members) > 1
        ):

            # Find user, if fail assign ''
            try:
                db_user = bot.db["users"].find_one(user_id=member.id)
            except Exception as e:
                print(e)
                return

            banned_users = [user["user_id"] for user in bot.db["banned_users"]]
            if member.id not in banned_users:
                await self.play_join_sound(member, db_user["join_sound"])

    @commands.command()
    @commands.guild_only()
    async def joinsound(
        self, ctx, join_sound=None, user_mentioned: discord.User = None
    ):
        table = bot.db["users"]

        # Check if user was mentioned
        if user_mentioned:
            db_user = table.find_one(user_id=user_mentioned.id)
            user = f"{user_mentioned.name}#{user_mentioned.discriminator}"
        else:
            user = f"{ctx.author.name}#{ctx.author.discriminator}"
            # Find user, if fail assign ''
            try:
                db_user = table.find_one(user_id=ctx.author.id)
            except Exception as e:
                db_user = None

        # Command passed without args
        if join_sound is None:
            if db_user is None:
                await ctx.send(as_code("You don't have a join sound."))
            else:
                await ctx.send(as_code(f"Your join sound is: {db_user['join_sound']}"))
            return

        # If no argument is passed, send the user their current join sound
        if join_sound == "remove":
            try:
                table.delete(user_id=ctx.author.id)
            except Exception as e:
                await ctx.send("Something went wrong...")
            await ctx.send(as_code("Your join sound has been removed"))
            return

        if not requests.head(utils.build_s3_url(join_sound)).ok:
            await ctx.send(as_code(f"Cannot find sound: {join_sound}.mp3"))
            return

        if user_mentioned:
            table.upsert(
                dict(user_id=user_mentioned.id, user=user, join_sound=join_sound),
                ["user_id"],
            )
            await ctx.send(
                as_code(
                    f"{user_mentioned.name}'s join sound has been set to {join_sound}"
                )
            )

        else:
            table.upsert(
                dict(user_id=ctx.message.author.id, user=user, join_sound=join_sound),
                ["user_id"],
            )
            await ctx.send(as_code(f"Your join sound has been set to {join_sound}"))


def setup(bot):
    bot.add_cog(Voice(bot))
